(function (window) {
    // Command used for recording:
    // export RCD=after; arecord -vv -fdat ./media/$RCD.wav; aplay ./media/$RCD.wav;
    var words = [
        // Pre-primer (40 words)
        { level: 'Pre-primer', text: 'a', media: 'media/a.wav' },
        { level: 'Pre-primer', text: 'and', media: 'media/and.wav' },
        { level: 'Pre-primer', text: 'away', media: 'media/away.wav' },
        { level: 'Pre-primer', text: 'big', media: 'media/big.wav' },
        { level: 'Pre-primer', text: 'blue', media: 'media/blue.wav' },
        { level: 'Pre-primer', text: 'can', media: 'media/can.wav' },
        { level: 'Pre-primer', text: 'come', media: 'media/come.wav' },
        { level: 'Pre-primer', text: 'down', media: 'media/down.wav' },
        { level: 'Pre-primer', text: 'find', media: 'media/find.wav' },
        { level: 'Pre-primer', text: 'for', media: 'media/for.wav' },
        { level: 'Pre-primer', text: 'funny', media: 'media/funny.wav' },
        { level: 'Pre-primer', text: 'go', media: 'media/go.wav' },
        { level: 'Pre-primer', text: 'help', media: 'media/help.wav' },
        { level: 'Pre-primer', text: 'here', media: 'media/here.wav' },
        { level: 'Pre-primer', text: 'I', media: 'media/i.wav' },
        { level: 'Pre-primer', text: 'in', media: 'media/in.wav' },
        { level: 'Pre-primer', text: 'is', media: 'media/is.wav' },
        { level: 'Pre-primer', text: 'it', media: 'media/it.wav' },
        { level: 'Pre-primer', text: 'jump', media: 'media/jump.wav' },
        { level: 'Pre-primer', text: 'little', media: 'media/little.wav' },
        { level: 'Pre-primer', text: 'look', media: 'media/look.wav' },
        { level: 'Pre-primer', text: 'make', media: 'media/make.wav' },
        { level: 'Pre-primer', text: 'me', media: 'media/me.wav' },
        { level: 'Pre-primer', text: 'my', media: 'media/my.wav' },
        { level: 'Pre-primer', text: 'not', media: 'media/not.wav' },
        { level: 'Pre-primer', text: 'one', media: 'media/one.wav' },
        { level: 'Pre-primer', text: 'play', media: 'media/play.wav' },
        { level: 'Pre-primer', text: 'red', media: 'media/red.wav' },
        { level: 'Pre-primer', text: 'run', media: 'media/run.wav' },
        { level: 'Pre-primer', text: 'said', media: 'media/said.wav' },
        { level: 'Pre-primer', text: 'see', media: 'media/see.wav' },
        { level: 'Pre-primer', text: 'the', media: 'media/the.wav' },
        { level: 'Pre-primer', text: 'three', media: 'media/three.wav' },
        { level: 'Pre-primer', text: 'to', media: 'media/to.wav' },
        { level: 'Pre-primer', text: 'two', media: 'media/two.wav' },
        { level: 'Pre-primer', text: 'up', media: 'media/up.wav' },
        { level: 'Pre-primer', text: 'we', media: 'media/we.wav' },
        { level: 'Pre-primer', text: 'where', media: 'media/where.wav' },
        { level: 'Pre-primer', text: 'yellow', media: 'media/yellow.wav' },
        { level: 'Pre-primer', text: 'you', media: 'media/you.wav' },

        // Primer (52 words)
        { level: 'Primer', text: 'all', media: 'media/all.wav' },
        { level: 'Primer', text: 'am', media: 'media/am.wav' },
        { level: 'Primer', text: 'are', media: 'media/are.wav' },
        { level: 'Primer', text: 'at', media: 'media/at.wav' },
        { level: 'Primer', text: 'ate', media: 'media/ate.wav' },
        { level: 'Primer', text: 'be', media: 'media/be.wav' },
        { level: 'Primer', text: 'black', media: 'media/black.wav' },
        { level: 'Primer', text: 'brown', media: 'media/brown.wav' },
        { level: 'Primer', text: 'but', media: 'media/but.wav' },
        { level: 'Primer', text: 'came', media: 'media/came.wav' },
        { level: 'Primer', text: 'did', media: 'media/did.wav' },
        { level: 'Primer', text: 'do', media: 'media/do.wav' },
        { level: 'Primer', text: 'eat', media: 'media/eat.wav' },
        { level: 'Primer', text: 'four', media: 'media/four.wav' },
        { level: 'Primer', text: 'get', media: 'media/get.wav' },
        { level: 'Primer', text: 'good', media: 'media/good.wav' },
        { level: 'Primer', text: 'have', media: 'media/have.wav' },
        { level: 'Primer', text: 'he', media: 'media/he.wav' },
        { level: 'Primer', text: 'into', media: 'media/into.wav' },
        { level: 'Primer', text: 'like', media: 'media/like.wav' },
        { level: 'Primer', text: 'must', media: 'media/must.wav' },
        { level: 'Primer', text: 'new', media: 'media/new.wav' },
        { level: 'Primer', text: 'no', media: 'media/no.wav' },
        { level: 'Primer', text: 'now', media: 'media/now.wav' },
        { level: 'Primer', text: 'on', media: 'media/on.wav' },
        { level: 'Primer', text: 'our', media: 'media/our.wav' },
        { level: 'Primer', text: 'out', media: 'media/out.wav' },
        { level: 'Primer', text: 'please', media: 'media/please.wav' },
        { level: 'Primer', text: 'pretty', media: 'media/pretty.wav' },
        { level: 'Primer', text: 'ran', media: 'media/ran.wav' },
        { level: 'Primer', text: 'ride', media: 'media/ride.wav' },
        { level: 'Primer', text: 'saw', media: 'media/saw.wav' },
        { level: 'Primer', text: 'say', media: 'media/say.wav' },
        { level: 'Primer', text: 'she', media: 'media/she.wav' },
        { level: 'Primer', text: 'so', media: 'media/so.wav' },
        { level: 'Primer', text: 'soon', media: 'media/soon.wav' },
        { level: 'Primer', text: 'that', media: 'media/that.wav' },
        { level: 'Primer', text: 'there', media: 'media/there.wav' },
        { level: 'Primer', text: 'they', media: 'media/they.wav' },
        { level: 'Primer', text: 'this', media: 'media/this.wav' },
        { level: 'Primer', text: 'too', media: 'media/too.wav' },
        { level: 'Primer', text: 'under', media: 'media/under.wav' },
        { level: 'Primer', text: 'want', media: 'media/want.wav' },
        { level: 'Primer', text: 'was', media: 'media/was.wav' },
        { level: 'Primer', text: 'well', media: 'media/well.wav' },
        { level: 'Primer', text: 'went', media: 'media/went.wav' },
        { level: 'Primer', text: 'what', media: 'media/what.wav' },
        { level: 'Primer', text: 'white', media: 'media/white.wav' },
        { level: 'Primer', text: 'who', media: 'media/who.wav' },
        { level: 'Primer', text: 'will', media: 'media/will.wav' },
        { level: 'Primer', text: 'with', media: 'media/with.wav' },
        { level: 'Primer', text: 'yes', media: 'media/yes.wav' },

        // 1st grade (41 words)
        { level: '1st grade', text: 'after', media: 'media/after.wav' },
        { level: '1st grade', text: 'again', media: 'media/again.wav' },
        { level: '1st grade', text: 'an', media: 'media/an.wav' },
        { level: '1st grade', text: 'any', media: 'media/any.wav' },
        { level: '1st grade', text: 'as', media: 'media/as.wav' },
        { level: '1st grade', text: 'ask', media: 'media/ask.wav' },
        { level: '1st grade', text: 'by', media: 'media/by.wav' },
        { level: '1st grade', text: 'could', media: 'media/could.wav' },
        { level: '1st grade', text: 'every', media: 'media/every.wav' },
        { level: '1st grade', text: 'fly', media: 'media/fly.wav' },
        { level: '1st grade', text: 'from', media: 'media/from.wav' },
        { level: '1st grade', text: 'give', media: 'media/give.wav' },
        { level: '1st grade', text: 'giving', media: 'media/giving.wav' },
        { level: '1st grade', text: 'had', media: 'media/had.wav' },
        { level: '1st grade', text: 'has', media: 'media/has.wav' },
        { level: '1st grade', text: 'her', media: 'media/her.wav' },
        { level: '1st grade', text: 'him', media: 'media/him.wav' },
        { level: '1st grade', text: 'his', media: 'media/his.wav' },
        { level: '1st grade', text: 'how', media: 'media/how.wav' },
        { level: '1st grade', text: 'just', media: 'media/just.wav' },
        { level: '1st grade', text: 'know', media: 'media/know.wav' },
        { level: '1st grade', text: 'let', media: 'media/let.wav' },
        { level: '1st grade', text: 'live', media: 'media/live.wav' },
        { level: '1st grade', text: 'may', media: 'media/may.wav' },
        { level: '1st grade', text: 'of', media: 'media/of.wav' },
        { level: '1st grade', text: 'old', media: 'media/old.wav' },
        { level: '1st grade', text: 'once', media: 'media/once.wav' },
        { level: '1st grade', text: 'open', media: 'media/open.wav' },
        { level: '1st grade', text: 'over', media: 'media/over.wav' },
        { level: '1st grade', text: 'put', media: 'media/put.wav' },
        { level: '1st grade', text: 'round', media: 'media/round.wav' },
        { level: '1st grade', text: 'some', media: 'media/some.wav' },
        { level: '1st grade', text: 'stop', media: 'media/stop.wav' },
        { level: '1st grade', text: 'take', media: 'media/take.wav' },
        { level: '1st grade', text: 'thank', media: 'media/thank.wav' },
        { level: '1st grade', text: 'them', media: 'media/them.wav' },
        { level: '1st grade', text: 'then', media: 'media/then.wav' },
        { level: '1st grade', text: 'think', media: 'media/think.wav' },
        { level: '1st grade', text: 'walk', media: 'media/walk.wav' },
        { level: '1st grade', text: 'were', media: 'media/were.wav' },
        { level: '1st grade', text: 'when', media: 'media/when.wav' },

        // 2nd grade (46 words)
        { level: '2nd grade', text: 'always', media: 'media/always.wav' },
        { level: '2nd grade', text: 'around', media: 'media/around.wav' },
        { level: '2nd grade', text: 'because', media: 'media/because.wav' },
        { level: '2nd grade', text: 'been', media: 'media/been.wav' },
        { level: '2nd grade', text: 'before', media: 'media/before.wav' },
        { level: '2nd grade', text: 'best', media: 'media/best.wav' },
        { level: '2nd grade', text: 'both', media: 'media/both.wav' },
        { level: '2nd grade', text: 'buy', media: 'media/buy.wav' },
        { level: '2nd grade', text: 'call', media: 'media/call.wav' },
        { level: '2nd grade', text: 'cold', media: 'media/cold.wav' },
        { level: '2nd grade', text: 'does', media: 'media/does.wav' },
        { level: '2nd grade', text: 'don\'t', media: 'media/dont.wav' },
        { level: '2nd grade', text: 'fast', media: 'media/fast.wav' },
        { level: '2nd grade', text: 'first', media: 'media/first.wav' },
        { level: '2nd grade', text: 'five', media: 'media/five.wav' },
        { level: '2nd grade', text: 'found', media: 'media/found.wav' },
        { level: '2nd grade', text: 'gave', media: 'media/gave.wav' },
        { level: '2nd grade', text: 'goes', media: 'media/goes.wav' },
        { level: '2nd grade', text: 'green', media: 'media/green.wav' },
        { level: '2nd grade', text: 'its', media: 'media/its.wav' },
        { level: '2nd grade', text: 'made', media: 'media/made.wav' },
        { level: '2nd grade', text: 'many', media: 'media/many.wav' },
        { level: '2nd grade', text: 'off', media: 'media/off.wav' },
        { level: '2nd grade', text: 'or', media: 'media/or.wav' },
        { level: '2nd grade', text: 'pull', media: 'media/pull.wav' },
        { level: '2nd grade', text: 'read', media: 'media/read.wav' },
        { level: '2nd grade', text: 'right', media: 'media/right.wav' },
        { level: '2nd grade', text: 'sing', media: 'media/sing.wav' },
        { level: '2nd grade', text: 'sit', media: 'media/sit.wav' },
        { level: '2nd grade', text: 'sleep', media: 'media/sleep.wav' },
        { level: '2nd grade', text: 'tell', media: 'media/tell.wav' },
        { level: '2nd grade', text: 'their', media: 'media/their.wav' },
        { level: '2nd grade', text: 'these', media: 'media/these.wav' },
        { level: '2nd grade', text: 'those', media: 'media/those.wav' },
        { level: '2nd grade', text: 'upon', media: 'media/upon.wav' },
        { level: '2nd grade', text: 'us', media: 'media/us.wav' },
        { level: '2nd grade', text: 'use', media: 'media/use.wav' },
        { level: '2nd grade', text: 'very', media: 'media/very.wav' },
        { level: '2nd grade', text: 'wash', media: 'media/wash.wav' },
        { level: '2nd grade', text: 'which', media: 'media/which.wav' },
        { level: '2nd grade', text: 'why', media: 'media/why.wav' },
        { level: '2nd grade', text: 'wish', media: 'media/wish.wav' },
        { level: '2nd grade', text: 'work', media: 'media/work.wav' },
        { level: '2nd grade', text: 'would', media: 'media/would.wav' },
        { level: '2nd grade', text: 'write', media: 'media/write.wav' },
        { level: '2nd grade', text: 'your', media: 'media/your.wav' },

        // 3rd grade (41 words)
        { level: '3rd grade', text: 'about', media: 'media/about.wav' },
        { level: '3rd grade', text: 'better', media: 'media/better.wav' },
        { level: '3rd grade', text: 'bring', media: 'media/bring.wav' },
        { level: '3rd grade', text: 'carry', media: 'media/carry.wav' },
        { level: '3rd grade', text: 'clean', media: 'media/clean.wav' },
        { level: '3rd grade', text: 'cut', media: 'media/cut.wav' },
        { level: '3rd grade', text: 'done', media: 'media/done.wav' },
        { level: '3rd grade', text: 'draw', media: 'media/draw.wav' },
        { level: '3rd grade', text: 'drink', media: 'media/drink.wav' },
        { level: '3rd grade', text: 'eight', media: 'media/eight.wav' },
        { level: '3rd grade', text: 'fall', media: 'media/fall.wav' },
        { level: '3rd grade', text: 'far', media: 'media/far.wav' },
        { level: '3rd grade', text: 'full', media: 'media/full.wav' },
        { level: '3rd grade', text: 'got', media: 'media/got.wav' },
        { level: '3rd grade', text: 'grow', media: 'media/grow.wav' },
        { level: '3rd grade', text: 'hold', media: 'media/hold.wav' },
        { level: '3rd grade', text: 'hot', media: 'media/hot.wav' },
        { level: '3rd grade', text: 'hurt', media: 'media/hurt.wav' },
        { level: '3rd grade', text: 'if', media: 'media/if.wav' },
        { level: '3rd grade', text: 'keep', media: 'media/keep.wav' },
        { level: '3rd grade', text: 'kind', media: 'media/kind.wav' },
        { level: '3rd grade', text: 'laugh', media: 'media/laugh.wav' },
        { level: '3rd grade', text: 'light', media: 'media/light.wav' },
        { level: '3rd grade', text: 'long', media: 'media/long.wav' },
        { level: '3rd grade', text: 'much', media: 'media/much.wav' },
        { level: '3rd grade', text: 'myself', media: 'media/myself.wav' },
        { level: '3rd grade', text: 'never', media: 'media/never.wav' },
        { level: '3rd grade', text: 'nine', media: 'media/nine.wav' },
        { level: '3rd grade', text: 'only', media: 'media/only.wav' },
        { level: '3rd grade', text: 'own', media: 'media/own.wav' },
        { level: '3rd grade', text: 'pick', media: 'media/pick.wav' },
        { level: '3rd grade', text: 'seven', media: 'media/seven.wav' },
        { level: '3rd grade', text: 'shall', media: 'media/shall.wav' },
        { level: '3rd grade', text: 'show', media: 'media/show.wav' },
        { level: '3rd grade', text: 'six', media: 'media/six.wav' },
        { level: '3rd grade', text: 'small', media: 'media/small.wav' },
        { level: '3rd grade', text: 'start', media: 'media/start.wav' },
        { level: '3rd grade', text: 'ten', media: 'media/ten.wav' },
        { level: '3rd grade', text: 'today', media: 'media/today.wav' },
        { level: '3rd grade', text: 'together', media: 'media/together.wav' },
        { level: '3rd grade', text: 'try', media: 'media/try.wav' },
        { level: '3rd grade', text: 'warm', media: 'media/warm.wav' },
    ];

    // // Focus on the pre-primer words for right now
    // window.learn.load(window.learn.shuffle(words.slice(0, 40)));
    window.learn.load(window.learn.shuffle(words));
})(window);
