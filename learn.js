(function (window, document) {
    var drawing = false;
    var mousePosition = { x: 0, y: 0 };
    var lastPosition = mousePosition;
    var canvas;
    var ctx;
    var MOUSE = 0, TOUCH = 1;

    var deck = [];
    var current = 0;
    var level = 'Pre-primer';

    function load(cards) {
        deck = cards;
        current = indexRange(deck, level)[0];
        show(current);
    }

    function indexRange(deck, level) {
        return deck.reduce((arr, card, index) => {
            if (card.level !== level) {
                return arr;
            }
            arr.push(index);
            return arr;
        }, []);
    }

    function show(index) {
        var indices = indexRange(deck, level);
        if (!indices.includes(index)) {
            return;
        }

        clearCanvas();
        var card = deck[index];
        if (card) {
            var text = document.getElementById('text');
            text.innerText = card.text || '??';
            var audio = document.getElementById('media');
            audio.src = card.media || '';
            var playButton = document.getElementById('play');
            playButton.style.display = card.media ? 'inline-block' : 'none';
            playButton.style.visibility = card.media ? 'visible' : 'hidden';
        }
    }

    function previous() {
        var indices = indexRange(deck, level);
        var idx = indices.findIndex((x) => x === current);
        if (current === -1 || idx === 0) {
            current = indices[indices.length - 1];
        } else {
            current = indices[idx - 1];
        }
        show(current);
    }

    function next() {
        var indices = indexRange(deck, level);
        var idx = indices.findIndex((x) => x === current);
        if (current === -1 || idx === indices.length - 1) {
            current = indices[0];
        } else {
            current = indices[idx + 1];
        }
        show(current);
    }

    function playMedia() {
        var audio = document.getElementById('media');
        audio.pause();
        audio.currentTime = 0;
        audio.play();
    }

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame
            || window.webkit.RequestAnimationFrame
            || window.mozRequestAnimationFrame
            || window.oRequestAnimationFrame
            || window.msRequestAnimationFrame
            || function (callback) {
                window.setTimeout(callback, 1000/60);
            }
    })();

    function getMousePosition(el, ev) {
        var rect = el.getBoundingClientRect();
        return {
            x: ev.clientX - rect.left,
            y: ev.clientY - rect.top
        }
    }

    function getTouchPosition(el, ev) {
        var rect = el.getBoundingClientRect();
        return {
            x: ev.touches[0].clientX - rect.left,
            y: ev.touches[0].clientY - rect.top
        }
    }

    function setupCanvas(el) {
        canvas.width = document.body.clientWidth;
        canvas.height = document.body.clientHeight;
        ctx = canvas.getContext('2d');
        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 24;
    }

    function drawEnabled(inputType, el, e) {
        drawing = true;
        mousePosition = inputType === MOUSE
            ? getMousePosition(el, e)
            : getTouchPosition(el, e);
        lastPosition = mousePosition;

        if (inputType === TOUCH) {
            try {
                el.dispatchEvent(e);
            } catch (ex) {
                console.log('Failed to dispatch event for drawEnable: ' + ex.message);
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
            }
        }
    }

    function drawDisabled(inputType, el, e) {
        drawing = false;
        if (inputType === TOUCH) {
            try {
                el.dispatchEvent(e);
            } catch (ex) {
                console.log('Failed to dispatch event for drawEnable: ' + ex.message);
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
            }
        }
    }

    function draw(inputType, el, e) {
        mousePosition = inputType === MOUSE
            ? getMousePosition(el, e)
            : getTouchPosition(el, e);

        if (inputType === TOUCH) {
            try {
                el.dispatchEvent(e);
            } catch (ex) {
                console.log('Failed to dispatch event for drawEnable: ' + ex.message);
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
            }
        }
    }

    function clearCanvas() {
        if (canvas) {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            mousePosition = { x: 0, y: 0 };
            lastPosition = mousePosition;
        }
    }

    function renderCanvas() {
        if (drawing && canvas) {
            if (lastPosition.x !== 0 && lastPosition.y !== 0) {
                ctx.beginPath();
                ctx.moveTo(lastPosition.x, lastPosition.y);
                ctx.lineTo(mousePosition.x, mousePosition.y);
                ctx.stroke();
                ctx.closePath();
            }
            lastPosition = mousePosition;
        }
    }

    function loop() {
        window.requestAnimFrame(loop);
        renderCanvas();
    }

    function shuffle(cards) {
        // Used to store a random number.
        var r;

        // Cutting the cards
        var left = [];
        var right = [];
        var center = [];
        for (var i = 0; i < cards.length; i++) {
            // Get something random and then push the current card in to one of
            // the heaps.
            r = Math.random() * 10;
            if (r < 3.5) {
                left.push(cards[i]);
            } else if (r < 7) {
                right.push(cards[i]);
            } else {
                center.push(cards[i]);
            }
        }

        // Put all the heaps back together to form the deck.
        var d = [];
        while (left.length || right.length || center.length) {
            r = Math.random() * 10;
            if (r < 3.5 && left.length) {
                d.push(left.pop());
            } else if (r < 7 && right.length) {
                d.push(right.pop());
            } else if (center.length) {
                d.push(center.pop());
            }
        }

        return d;
    }

    function setLevel(value) {
        level = value;
        current = indexRange(deck, level)[0];
    }

    (function () {
        var i;

        var item = document.getElementById('card');
        item.onclick = function() {
            playMedia();
        }
        var playButton = document.getElementById('play');
        playButton.onclick = function() {
            playMedia();
        }
        var prevButton = document.getElementById('prev');
        prevButton.onclick = function() {
            previous();
        }
        var nextButton = document.getElementById('next');
        nextButton.onclick = function() {
            next();
        }
        var clearButton = document.getElementById('clear');
        clearButton.onclick = function() {
            clearCanvas();
        }

        // Level selection
        var optionLevel = document.getElementById('level');
        optionLevel.onchange = function () {
            setLevel(optionLevel.value);
            next();
        }

        canvas = document.getElementById('canvas');
        setupCanvas(canvas);

        // Drawing with a mouse
        canvas.addEventListener('mousedown', function (e) { drawEnabled(MOUSE, canvas, e); }, false)
        canvas.addEventListener('mouseup',   function (e) { drawDisabled(MOUSE, canvas, e); }, false)
        canvas.addEventListener('mouseout',   function (e) { drawDisabled(MOUSE, canvas, e); }, false)
        canvas.addEventListener('mousemove', function (e) { draw(MOUSE, canvas, e); }, false)

        // Drawing with touch
        canvas.addEventListener('touchstart', function (e) { drawEnabled(TOUCH, canvas, e); }, false)
        canvas.addEventListener('touchend',   function (e) { drawDisabled(TOUCH, canvas, e); }, false)
        canvas.addEventListener('touchmove',  function (e) { draw(TOUCH, canvas, e); }, false)

        // Prevent scrolling when touching the canvas
        document.body.addEventListener('touchstart', function (e) { if (e.target === canvas) { e.preventDefault() } }, false)
        document.body.addEventListener('touchend',   function (e) { if (e.target === canvas) { e.preventDefault() } }, false)
        document.body.addEventListener('touchmove',  function (e) { if (e.target === canvas) { e.preventDefault() } }, false)

        loop();
    })();

    window.learn = {
        load: load,
        show: show,
        previous: previous,
        next: next,
        play: playMedia,
        clear: clearCanvas,
        shuffle: shuffle,
    };
})(window, document);
